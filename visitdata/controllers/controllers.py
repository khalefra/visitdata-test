from collections import defaultdict

from managers.managers import (PokeAPIManager, DataManager, APIException,
                               DataException)
from utils.utils import PrettyPrinter


class ControllerException(Exception):
    """
    Custom exception class to manage different errors
    during the main process.
    """

    def __init__(self, msg):
        super(Exception, self).__init__()
        self.msg = msg


class MainController:
    """
    Main class used to load and process data from the pokemon api.
    Workflow is as follow:
    ---> init
    ---> run
        ---> load data from the API.
        ---> get the results.
        ---> save log.
    """

    def __init__(self, generation, key_filter, save_log=False):
        self.generation = generation
        self.versions = []
        self.key_filter = key_filter
        self.save_log = save_log
        self.printer = PrettyPrinter()
        self.api_manager = PokeAPIManager()
        self.data_manager = DataManager()

    def skip(self, msg):
        """
        Skip the current process and raise a ControllerException.
        :param msg: message to show to the user when an exception is raised.
        """
        self.printer.pretty_print(msg, mode=PrettyPrinter.EXCEPTION)
        raise ControllerException(msg)

    def skip_api_error(self, msg):
        return self.skip("API ERROR: {}".format(msg))

    def skip_data_error(self, msg):
        return self.skip("DATA ERROR: {}".format(msg))

    def run(self):
        """
        Main process. Get the versions of the generation than load the full data
        and filter by versions. At the same time messages are displayed on the
        console to show the progress of the process.
        """
        try:
            try:
                # 0 :: Get the versions of the generation
                self.versions = self.api_manager.get_versions(self.generation)
                self.printer.pretty_print_header('Versions')
                self.printer.pretty_print(
                    {index: value for index, value in enumerate(self.versions)},
                    mode=PrettyPrinter.INFO
                )

                # 1 :: Loading Data.
                self.printer.pretty_print_header('Loading Data')
                self.load_initial_data()
                self.printer.pretty_print_header('Loading Done')

                # 2 :: Filtering by versions of the generation.
                self.filter_by_versions()

                if self.data_manager.data_frame.empty:
                    self.skip_data_error(
                        "No pokemon for generation %s" % self.generation)

                # 3 :: Retrieving results.
                results = self.retrieve_results()
                self.printer.pretty_print_header(
                    'Number of pokemon with higher {} than average by type'
                    .format(self.key_filter)
                )
                self.printer.pretty_print(results, mode=PrettyPrinter.INFO)

            except APIException as e:
                self.skip_api_error(e.msg)

            except DataException as e:
                self.skip_data_error(e.msg)
        except ControllerException:
            pass

    def load_initial_data(self):
        """
        Load the initial data of the data_manager.
        """
        self.data_manager.load_data(self.api_manager.get_pokemon_list())

    def filter_by_versions(self):
        """
        Filter the data_frame of the data_manger with the pokemon of the
        specified versions from the selected generation.
        """
        filtered_list = []
        for index, row in self.data_manager.data_frame.iterrows():
            if self.is_pokemon_from_versions(row, self.versions):
                filtered_list.append(row)
        self.data_manager.load_data(filtered_list)

    @classmethod
    def is_pokemon_from_versions(cls, pokemon, versions):
        """
        Return True if the pokemon is
        :param pokemon: dict or row from data frame following the structure of
        Pokeapi pokemon objects.
        :param versions: list of the names of the versions to filter on.
        :return: A bool.
        """
        game_indices = pokemon['game_indices']
        for game_index in game_indices:
            if game_index['version']['name'] in versions:
                return True
        return False

    def retrieve_results(self):
        """
        Get the result of the number of pokemon with higher value than average
        by type.
        :return: A dict.
        """
        key_mean = self.data_manager.get_mean(self.key_filter)
        result = defaultdict(int)
        for index, row in self.data_manager.data_frame.iterrows():
            if row[self.key_filter] > key_mean:
                for pokemon_type in row['types']:
                    result[pokemon_type['type']['name']] += 1

        return result
