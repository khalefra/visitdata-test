class PrettyPrinter:
    """
    Utils class to pretty print the information of this project.
    It allows to pretty print json data and exceptions on the console.
    """

    INFO = 'INFO'
    EXCEPTION = 'EXCEPTION'

    def __init__(self):
        self.mode_mapping = {
            self.INFO: 'get_pretty_info',
            self.EXCEPTION: 'get_pretty_exception',
        }

    @classmethod
    def get_pretty_info(cls, json_dict=None):
        pretty_string = "*********** INFO ***********\n"
        if type(json_dict) is str:
            return pretty_string + json_dict

        for key, value, in json_dict.items():
            pretty_string += "{}: {}\n".format(key, value)
        pretty_string += "***************************\n"
        return pretty_string

    @classmethod
    def get_pretty_exception(cls, exception=''):
        pretty_string = "*********** Exception ***********\n"
        return pretty_string + exception

    def get_method_by_mapping(self, mode='INFO'):
        method_name = self.mode_mapping.get(mode, 'get_pretty_info')
        return getattr(self, method_name)

    @classmethod
    def get_pretty_header(cls, header_string):
        return "------------------ {} ------------------\n".format(
            header_string
        )

    def pretty_print_header(self, header_string):
        print(self.get_pretty_header(header_string))

    def pretty_print(self, data=None, mode='INFO'):
        method = self.get_method_by_mapping(mode)
        text = method(data)
        print(text)
