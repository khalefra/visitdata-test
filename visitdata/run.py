"""
Main script used as a simple python command.
Example :
$ python run.py 1 -k weight -s
"""
import argparse

from controllers.controllers import MainController

parser = argparse.ArgumentParser(description='Visit Data Test')
parser.add_argument('generation', help='generation of pokemon to analyse')
parser.add_argument(
    '-k', '--key_filter', dest='key_filter', type=str, default="weight",
    help='key to filter on, weight by default'
)
parser.add_argument(
    "-s", '--save_log', dest='save_log', action='store_true', default=False,
    help='save analysis in a log file'
)

args = parser.parse_args()
main_controller = MainController(
    args.generation, args.key_filter, save_log=args.save_log)
main_controller.run()
