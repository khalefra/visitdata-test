"""
Default project settings. Override these settings if you want
to change base url and the endpoints of the API.
"""

import os

# Log Directory
BASE_DIR = os.path.dirname(os.path.realpath(__file__))
LOGS_DIR = os.path.join(BASE_DIR, "logs")


# BASE URL
BASE_API_URL = "https://pokeapi.co/api/v2/"

# ENDPOINTS
POKEMON_URL = "pokemon/?limit=9999"
GENERATION_URL = "generation/"
