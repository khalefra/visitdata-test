import urllib.parse

import pandas
import requests

from settings import BASE_API_URL, POKEMON_URL, GENERATION_URL


class APIException(Exception):
    """
    Custom exception class to manage different errors
    for the APIManager.
    """

    def __init__(self, msg):
        super(Exception, self).__init__()
        self.msg = msg


class DataException(Exception):
    """
    Custom exception class to manage different errors
    for the DataManager.
    """

    def __init__(self, msg):
        super(Exception, self).__init__()
        self.msg = msg


class PokeAPIManager:
    """
    Simple manager for the pokeapi Api.
    """

    def get_pokemon_list(self):
        """
        Get the full list of pokemon.
        :return: A list of dictionaries.
        """
        response = requests.get(self.get_pokemon_url())
        self.check_status(response, error_msg="Unable to get pokemon list")
        try:
            pokemon_resources = response.json()['results']
            pokemon_list = []

            for resource in pokemon_resources:
                pokemon_response = requests.get(resource['url'])
                if pokemon_response.status_code == 200:
                    pokemon_list.append(pokemon_response.json())
            return pokemon_list
        except KeyError:
            raise APIException("Cannot get the pokemon list")

    def get_versions(self, generation):
        """
        Get the versions of the specified :generation:
        Example: For generation 1, ['red', 'blue'] will be returned.
        :param generation: Index of the generation
        :return: A list.
        """
        response = requests.get(
            urllib.parse.urljoin(self.get_generation_url(), generation)
        )
        self.check_status(
            response, error_msg="No versions for generation %s." % generation)

        versions = []
        for version_group_resource in response.json()['version_groups']:
            version_group_response = requests.get(version_group_resource['url'])
            if version_group_response.status_code == 200:
                for version in version_group_response.json()['versions']:
                    versions.append(version.get('name'))
        return versions

    @classmethod
    def get_generation_url(cls):
        """
        Get the full url endpoint for the version_group.
        :return: A string (url).
        """
        return urllib.parse.urljoin(BASE_API_URL, GENERATION_URL)

    @classmethod
    def get_pokemon_url(cls):
        """
        Get the full url endpoint for the version_group.
        :return: A string (url).
        """
        return urllib.parse.urljoin(BASE_API_URL, POKEMON_URL)

    @classmethod
    def check_status(cls, response, error_msg=None):
        """
        Check if the response status is 200 and raise an APIException if not.
        :param response: A requests response object.
        :param error_msg: The error_message to raise if status is not 200.
        :return:
        """
        if not response.status_code == 200:
            raise APIException(
                error_msg or "Request returned code %s" % response.status_code)


class DataManager:
    """
    Data manager for
    """
    def __init__(self):
        super(DataManager, self).__init__()
        self.data_frame = None

    def load_data(self, data):
        """
        Set the data_frame with the data parameter.
        :param data: A list ot dict or any type supported by the DataFrame
        constructor.
        :return:
        """
        self.data_frame = pandas.DataFrame(data)

    def get_mean(self, key):
        """
        Get the mean value of a column for the data frame.
        :param key: Column name.
        :return: A float.
        """
        try:
            return self.data_frame[key].mean()
        except KeyError:
            raise DataException("%s is not an attribute of a pokemon" % key)
        except TypeError:
            raise DataException("%s is not a numeric value" % key)
