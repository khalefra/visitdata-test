Simple Python Test for Visit Data

Example with results:
$ python run.py 1
------------------ Versions ------------------

*********** INFO ***********
0: red
1: blue
2: yellow
***************************

------------------ Loading Data ------------------

------------------ Loading Done ------------------

------------------ Number of pokemon with higher weight than average by type ------------------

*********** INFO ***********
poison: 6
grass: 2
flying: 10
fire: 4
water: 12
ground: 7
fighting: 5
psychic: 8
rock: 6
steel: 1
electric: 3
normal: 5
ice: 4
fairy: 1
bug: 2
dragon: 1
***************************
